package main

import (
	"bufio"
	"fmt"
	"os"

	. "bitbucket.org/Agif/loan_proccess_aprela/logics"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("use snake case (ex : my_name) for each string argument \n")

	for {
		fmt.Print("$ ")
		cmdString, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		err = RunCommand(cmdString)
		if err != nil {
			fmt.Println(err)
		}
	}
}
