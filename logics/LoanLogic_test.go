package logics_test

import (
	"testing"

	. "bitbucket.org/Agif/loan_proccess_aprela/logics"
)

func TestRunCommand(t *testing.T) {
	type args struct {
		inputStr string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := RunCommand(tt.args.inputStr); (err != nil) != tt.wantErr {
				t.Errorf("RunCommand() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
