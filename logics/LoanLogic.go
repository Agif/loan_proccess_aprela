package logics

import (
	"errors"
	"os"
	"strconv"
	"strings"
)

func RunCommand(inputStr string) error {
	inputStr = strings.TrimSuffix(inputStr, "\n")
	arrInputStr := strings.Fields(inputStr)

	typeInput := arrInputStr[0]

	switch typeInput {
	case "exit":
		os.Exit(0)

	case "create_day_max":

		if len(arrInputStr) != 2 {
			return errors.New("Error!, Need max request argument \n 'create_day_max <value>, ex: create_day_max 50'")
		}

		checkInput, _ := strconv.Atoi(arrInputStr[1])

		if checkInput <= 0 {
			return errors.New("Your input not valid!, Please input again with digit more than 0 (zero), ex: 50 \n 'create_day_max <value>'")
		}

		setMax := SetLimitRequest(arrInputStr[1])

		if setMax != nil {
			return setMax
		}

		return errors.New("Success set limit request this today to " + arrInputStr[1])

	case "add":
		if len(arrInputStr) != 7 {
			return errors.New("Error!, Need argument NIK, Name, Gender, Province, Age, Amount \n example format : add 2107110711071107 Jingga L DKI_JAKARTA 24 2000000 \n use underscore instead of whitespace")
		}

		check := Validate(arrInputStr)

		if check != nil {
			return check
		}

		err, ids := SaveData(arrInputStr)

		if err != nil {
			return err
		}

		return errors.New("success " + ids)

	case "status":
		if len(arrInputStr) != 2 {
			return errors.New("need Loan ID as argument, ex: status 2107110711071107NameL")
		} else {
			return errors.New(GetStatusByLoanID(arrInputStr[1]))
		}

	case "show_all":
		ShowAll()

	case "installment":
		if len(arrInputStr) != 3 {
			return errors.New("need Loan ID and Multiple as argument \n example format: installment 05111901 03")
		}

		value, _ := strconv.Atoi(arrInputStr[2])
		Installment(arrInputStr[1], value)

	case "find_by_rejected_amount":
		if len(arrInputStr) != 2 {
			return errors.New("need loan amount as second argument  \n example format : find_by_rejected_amount (amount)")
		}

		FindAmountStatus(arrInputStr[1], "Rejected")

	case "find_by_accepted_amount":
		if len(arrInputStr) != 2 {
			return errors.New("need loan amount as second argument \n example format : find_by_accepted_amount (amount)")
		}

		FindAmountStatus(arrInputStr[1], "Accepted")

	case "set_status":
		if len(arrInputStr) != 3 {
			return errors.New("Set status not valid command \n example format : set_status <NIK> <value:Accepted|Rejected> ex: set_status 3309330912340987 Accepted")
		}

		nik := arrInputStr[1]
		status := arrInputStr[2]

		set := SetStatus(nik, status)

		if set != nil {
			return set
		}

		return errors.New("data with this nik:" + nik + ", status has been successfully converted to " + status)

	default:
		return errors.New("Command available is: \n\t create_day_max : to set maximum request/day \n\t add : to add new loan request \n\t show_all : to show all requested loan \n\t status : to check status of a loanID \n\t set_status to update status Loan \n\t find_by_rejected_amount : to find rejected loanID amount \n\t find_by_accepted_amount : to find accepted loanID by amount \n\t exit : to close the app")
	}

	return nil
}
