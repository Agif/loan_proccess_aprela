package logics

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Loans struct {
	NIK, Name, Gender, Province, Status, LoanId string
	Age, Amount                                 int
	CreatedAt                                   string
}

type Transaction struct {
	LimitDay int
	Date     string
	Data     []Loans
}

var processedDataLoans = make(map[string]map[string]string)

var dummyjson = "logics/dummy.json"
var ReadFile, _ = ioutil.ReadFile(dummyjson)
var FileToStruct = json.Unmarshal([]byte(ReadFile), &transactions)

var times = time.Now()

var transactions Transaction
var loans []Loans

func Validate(field []string) error {
	if FileToStruct != nil {
		return errors.New("Error when read the database")
	}

	if len(transactions.Data) >= transactions.LimitDay {
		limit := strconv.Itoa(transactions.LimitDay)
		return errors.New("the daily lending limit has reached " + limit + " requests for today")
	}

	NIK := field[1]
	Name := field[2]
	Gender := field[3]
	Province := field[4]
	Age := field[5]
	Amount := field[6]

	vktp := regexp.MustCompile("^[0-9]{16}$")
	vgender := regexp.MustCompile("^L$|^P$")
	vage := regexp.MustCompile("[0-9]{0,2}")
	vplace := regexp.MustCompile("^DKI_JAKARTA$|^JAWA_BARAT$|^JAWA_TIMUR$|^SUMATRA_UTARA$")

	if NIK == "" {
		return errors.New("Required NIK(Nomor Induk KTP)")
	}

	if vktp.MatchString(NIK) == false {
		return errors.New("NIK must be a number and total 16 digits")
	}

	for _, value := range transactions.Data {
		if value.NIK == NIK {
			return errors.New("NIK Already Taken")
		}
	}

	if Name == "" {
		return errors.New("Required Name")
	}

	if Gender == "" {
		return errors.New("Required Gender")
	}

	if vgender.MatchString(Gender) == false {
		return errors.New("Gender options are only available 'L' or 'P'")
	}

	if Province == "" {
		return errors.New("Required Place")
	}

	if vplace.MatchString(strings.ToUpper(Province)) == false {
		return errors.New("Place options are only available 'DKI_JAKARTA', 'JAWA_BARAT', 'JAWA_TIMUR', 'SUMATERA_UTARA'")
	}

	if Age == "" {
		return errors.New("Required Age")
	}

	if vage.MatchString(Age) == false {
		return errors.New("Age must be a number, min 1 digit, max 2 digit")
	}

	intAge, _ := strconv.Atoi(Age)
	if intAge < 17 || intAge > 80 {
		return errors.New("Allowable age from 17 years to 80 years")
	}

	if Amount == "" {
		return errors.New("Required Amount")
	}

	intAmount, _ := strconv.Atoi(Amount)
	if intAmount < 1000000 || intAmount > 10000000 {
		return errors.New("Allowable loans only number from 1 million to 10 million, and no dots")
	}

	if intAmount%1000000 != 0 {
		return errors.New("Amount Value must be a multiple of 1 million, for example: 1000000, 2000000")
	}

	return nil
}

func SetLimitRequest(limit string) error {
	limitRequest, _ := strconv.Atoi(limit)

	transactions := new(Transaction)
	transactions.LimitDay = limitRequest
	transactions.Date = times.Format(time.RFC1123)

	file, err := json.MarshalIndent(transactions, "", " ")

	ioutil.WriteFile(dummyjson, file, 0644)

	if err != nil {
		return err
	}

	return nil
}

func ShowAll() {
	if FileToStruct != nil {
		fmt.Println("Error when read the database")
	}

	myJSON, _ := json.MarshalIndent(transactions, "", " ")
	fmt.Println(string(myJSON))
}

func SetStatus(nik, status string) error {
	if FileToStruct != nil {
		return errors.New("Error when read the database")
	}

	if status != "Accepted" && status != "Rejected" {
		return errors.New("status value is not correct, only use the value: Rejected or Accepted")
	}

	msg := false
	for i, value := range transactions.Data {
		if value.NIK == nik {
			transactions.Data[i].Status = status
			msg = true
		}
	}

	if msg != true {
		return errors.New("Update Fail, Data with nik: " + nik + " not found")
	}

	WriteFile, err := json.MarshalIndent(transactions, "", " ")

	ioutil.WriteFile(dummyjson, WriteFile, 0644)

	if err != nil {
		return err
	}

	return nil
}

func FindAmountStatus(Amount, Status string) {
	if FileToStruct != nil {
		fmt.Println("Error when read the database")
	}

	var result strings.Builder

	for _, val := range transactions.Data {

		if strconv.Itoa(val.Amount) == Amount && val.Status == Status {
			result.WriteString(val.LoanId + " ")
		}
	}

	if result.String() == "" {
		result.WriteString("Your search not found")
	}

	fmt.Println(result.String())
}

func GetStatusByLoanID(LoanId string) string {
	if FileToStruct != nil {
		return "Error when read the database"
	}

	response := "Loan ID " + LoanId + " not found"

	for _, value := range transactions.Data {
		if value.LoanId == LoanId {
			response = "Loan ID " + LoanId + " is " + value.Status
		}
	}

	return response
}

func SaveData(field []string) (error, string) {
	NIK := field[1]
	Name := field[2]
	Gender := field[3]
	Province := field[4]
	Age, _ := strconv.Atoi(field[5])
	Amount, _ := strconv.Atoi(field[6])
	LoanId := NIK + Name + Gender

	transactions.Data = append(transactions.Data, Loans{
		NIK:       NIK,
		Name:      Name,
		Gender:    Gender,
		Province:  Province,
		Age:       Age,
		Amount:    Amount,
		LoanId:    LoanId,
		CreatedAt: times.Format(time.RFC1123),
		Status:    "Pending",
	})

	WriteFile, err := json.MarshalIndent(transactions, "", " ")

	ioutil.WriteFile(dummyjson, WriteFile, 0644)

	if err != nil {
		return err, LoanId
	}

	return nil, LoanId
}

func Installment(key string, multiple int) {
	var table strings.Builder
	table.WriteString("--------------------------------------------------------------\n")
	table.WriteString("Month \t|  DueDate  | AdministrationFee |  Capital  |  Total  \n")
	table.WriteString("--------------------------------------------------------------\n")

	admin := 100000
	adminFee := fmt.Sprintf("%d", admin)

	multi := fmt.Sprintf("%d", multiple)
	total := fmt.Sprintf("%d", multiple+admin)

	for index := 1; index <= multiple; index++ {
		table.WriteString("0" + strconv.Itoa(index) + " \t|  " + times.Format("020106") + "   |\t " + adminFee + " \t|      " + multi + "    |  " + total)
		table.WriteString("\n")
	}

	fmt.Println(table.String())
}
