package logics_test

import (
	"testing"

	. "bitbucket.org/Agif/loan_proccess_aprela/logics"
)

func TestValidate(t *testing.T) {
	type args struct {
		field []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Validate(tt.args.field); (err != nil) != tt.wantErr {
				t.Errorf("Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSetLimitRequest(t *testing.T) {
	type args struct {
		limit string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SetLimitRequest(tt.args.limit); (err != nil) != tt.wantErr {
				t.Errorf("SetLimitRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestShowAll(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ShowAll()
		})
	}
}

func TestSetStatus(t *testing.T) {
	type args struct {
		nik    string
		status string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SetStatus(tt.args.nik, tt.args.status); (err != nil) != tt.wantErr {
				t.Errorf("SetStatus() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFindAmountStatus(t *testing.T) {
	type args struct {
		Amount string
		Status string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			FindAmountStatus(tt.args.Amount, tt.args.Status)
		})
	}
}

func TestGetStatusByLoanID(t *testing.T) {
	type args struct {
		LoanId string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetStatusByLoanID(tt.args.LoanId); got != tt.want {
				t.Errorf("GetStatusByLoanID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSaveData(t *testing.T) {
	type args struct {
		field []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err, got := SaveData(tt.args.field)
			if (err != nil) != tt.wantErr {
				t.Errorf("SaveData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("SaveData() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInstallment(t *testing.T) {
	type args struct {
		key      string
		multiple int
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Installment(tt.args.key, tt.args.multiple)
		})
	}
}
